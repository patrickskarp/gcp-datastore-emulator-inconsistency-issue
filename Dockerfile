FROM microsoft/dotnet:2.1-sdk

WORKDIR /app

COPY ./gcp-datastore-emulator-inconsistency-issue.sln ./
COPY ./gcp-datastore-emulator-inconsistency-issue.csproj ./

RUN dotnet restore

COPY ./ ./

RUN dotnet build

CMD ["dotnet", "test"]
