using Google.Cloud.Datastore.V1;
using Xunit;

namespace gcp_datastore_emulator_inconsistency_issue
{
    public class SomeTests : TestBase
    {
        #region Prepare data
        private void PrepareData(string kind)
        {
            EnsureEmptyKind(kind);
            
            Insert(new Entity
            {
                Key = GetKey(kind, 1),
                ["String"] = "one",
                ["X"] = 3
            });
            
            Insert(new Entity
            {
                Key = GetKey(kind, 2),
                ["String"] = "two",
                ["X"] = 3
            });
            
            Insert(new Entity
            {
                Key = GetKey(kind, 3),
                ["String"] = "three",
                ["X"] = 5
            });
            
            Insert(new Entity
            {
                Key = GetKey(kind, 4),
                ["String"] = "four",
                ["X"] = 6
            });
            
            Insert(new Entity
            {
                Key = GetKey(kind, 5),
                ["String"] = "five",
                ["X"] = 4
            });
        }
        #endregion

        [Fact]
        public async void Query()
        {
            PrepareData("DonkeyDiamonds");
            
            var actual = await GetDb().RunQueryAsync(new Query("DonkeyDiamonds")
            {
                Filter = Filter.LessThanOrEqual("X", new Value {IntegerValue = 4})
            });
            
            Assert.Equal(3, actual.Entities.Count);
            Assert.Contains(actual.Entities, x => x.Key.Path[0].Id == 1 && x["String"].StringValue == "one");
            Assert.Contains(actual.Entities, x => x.Key.Path[0].Id == 2 && x["String"].StringValue == "two");
            Assert.Contains(actual.Entities, x => x.Key.Path[0].Id == 5 && x["String"].StringValue == "five");
        }
        
        [Fact]
        public async void GqlQuery()
        {
            PrepareData("BagOfBaguettes");
            
            var actual = await GetDb().RunQueryAsync(new GqlQuery
            {
                AllowLiterals = true,
                QueryString = "select * from BagOfBaguettes where X<=4"
            });
            
            Assert.Equal(3, actual.Entities.Count);
            Assert.Contains(actual.Entities, x => x.Key.Path[0].Id == 1 && x["String"].StringValue == "one");
            Assert.Contains(actual.Entities, x => x.Key.Path[0].Id == 2 && x["String"].StringValue == "two");
            Assert.Contains(actual.Entities, x => x.Key.Path[0].Id == 5 && x["String"].StringValue == "five");
        }

        [Fact]
        public async void Query_projection()
        {
            PrepareData("Full");
            
            var actual = await GetDb().RunQueryAsync(new Query("Full")
            {
                Filter = Filter.LessThanOrEqual("X", new Value {IntegerValue = 4}),
                Projection = { "__key__", "String" }
            });
            
            Assert.Equal(3, actual.Entities.Count);
            Assert.Contains(actual.Entities, x => x.Key.Path[0].Id == 1 && x["String"].StringValue == "one");
            Assert.Contains(actual.Entities, x => x.Key.Path[0].Id == 2 && x["String"].StringValue == "two");
            Assert.Contains(actual.Entities, x => x.Key.Path[0].Id == 5 && x["String"].StringValue == "five");
        }
    }
}