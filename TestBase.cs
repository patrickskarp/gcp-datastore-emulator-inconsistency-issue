using System;
using System.Linq;
using Google.Cloud.Datastore.V1;
using Grpc.Core;

namespace gcp_datastore_emulator_inconsistency_issue
{
    public abstract class TestBase
    {
        private DatastoreDb db;
        private bool isConnected = false;

        protected virtual string GetProjectId()
        {
            return "my-project";
        }
        
        protected virtual string GetConnectionString()
        {
            return Environment.GetEnvironmentVariable("DATASTORE_HOST") ?? "localhost:8081";
        }
        
        protected virtual string GetNamespace()
        {
            return GetType().Name;
        }

        protected virtual void Connect()
        {
            if (!isConnected)
            {
                isConnected = true;
                db = DatastoreDb.Create(GetProjectId(), GetNamespace(), GetClient());
                
                // truncate the namespace ... aka remove all kinds
                var query = new Query("__kind__") { Limit = int.MaxValue };

                foreach (var kindEntity in db.RunQuery(query).Entities)
                {
                    EnsureEmptyKind(kindEntity.Key.Path[0].Name);
                }
            }
        }

        private Uri ToUri(string url)
        {
            var uri = new Uri(url);

            if (string.IsNullOrEmpty(uri.Host))
            {
                return new Uri($"fake://{url}");
            }

            return uri;
        }

        protected virtual DatastoreClient GetClient()
        {
            var url = ToUri(GetConnectionString());
            
            return DatastoreClient.Create(
                new Channel(url.Host, url.Port, ChannelCredentials.Insecure),
                DatastoreSettings.GetDefault()
            );
        }

        protected virtual DatastoreDb GetDb()
        {
            Connect();
            return db;
        }

        protected void EnsureEmptyKind(string kind)
        {
            var query = new Query(kind)
            {
                Limit = int.MaxValue,
                Projection = {new Projection("__key__")}
            };

            var res = GetDb().RunQuery(query);
            
            GetDb().Delete(res.Entities.Select(x => x.Key));
        }

        protected virtual void Insert(Entity entity)
        {
            GetDb().Insert(entity);
        }

        protected virtual Key GetKey(string kind, long id)
        {
            return GetDb().CreateKeyFactory(kind).CreateKey(id);
        }
    }
}