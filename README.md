# Google Cloud Datastore emulator inconsistency issue

[The issue in the official issue tracker](https://issuetracker.google.com/issues/111182297)

First of all, **this issue is intermittent** - i.e. aggressively annoying!

I experience this issue when running automated tests that work with the emulator.

My tests follow a pattern of:
1. remove everything
2. insert what I need for _this_ test
3. run the queries etc. and assert stuff

The problem is that the tests will _sometimes_ fail on assertions by finding too few or too many entities.

I have only used the emulator from dotnet core, so it might be an SDK problem.

I have ensured that my tests are running 1 at a time.

I have tried inserting sleeps (including looooong ones - e.g. 10 seconds) in various places in the hope that the emulator just could not keep up, but to no avail.


## Running stuff

`$ docker-compose -up` will setup the datastore emulator + a ui for it. It also builds and runs the tests.
